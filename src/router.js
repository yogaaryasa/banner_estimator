import Vue from "vue";
import Router from "vue-router";
import Starter from "./views/Starter.vue";
import Banners from "./views/Banners.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "starter",
      component: Starter
    },
    {
      path: "/banners",
      name: "Banners",
      component: Banners
    }
  ]
});
